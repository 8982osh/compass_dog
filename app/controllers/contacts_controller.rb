class ContactsController < ApplicationController
  def index
  end

  def show
  end

  def edit
  end

  def new
  	@contact = Contact.new
  end

  def create
  	@contact = Contact.new(contact_params)
  	if @contact.save
      ContactMailer.new_contact(@contact).deliver_now
  		redirect_to @contact 
  	else
      flash[:error] = @contact.errors.full_messages
      render :new
    end
  end


  private
    def contact_params
  	  params.require(:contact).permit(:first_name, :last_name, :email, :subject, 
      :description)
    end
end
