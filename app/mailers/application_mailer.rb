class ApplicationMailer < ActionMailer::Base

  default from: "compass@example.com"
  layout 'mailer'

end
