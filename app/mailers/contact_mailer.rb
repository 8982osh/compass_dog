class ContactMailer < ApplicationMailer

  def new_contact(contact)

    mail to: contact.email, subject: "Confirmation Email"
  end
end
