class Contact < ActiveRecord::Base
  
  before_save :capitalize_name

  validates :first_name, 
            length: { maximum: 30 }, 
            presence: true
  validates :last_name, 
            length: { maximum: 30 }, 
            presence: true
  validates :subject, presence: true
  VALID_EMAIL_REGEX = /\A[\w+\-.]+@[a-z\d\-]+(\.[a-z\d\-]+)*\.[a-z]+\z/i
  validates :email, 
            presence: true, 
            length: { maximum: 100 }, 
            format: { with: VALID_EMAIL_REGEX }
  validates :description, presence: true

private
  #Capitalize first and last names
  def capitalize_name
  	self.first_name.capitalize! 
    self.last_name.capitalize!  
  end
end
