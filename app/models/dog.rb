class Dog < ActiveRecord::Base 

  #Filter status decides which view dogs will be displayed on
  scope :foster, -> { where status: 'Foster' }
  scope :adoptable, -> { where status: 'Adoptable' }
  scope :pending, -> { where status: 'Pending' }
  scope :homed, -> { where status: 'Homed' }
  scope :expired, -> { where status: 'Expired' }

  before_save :set_breedtype, :capitalize_dog_name

  attr_accessor :status_choice, :gender_choice, :age_choice, :breed_choice, :cat_choice, :dog_choice, :human_choice, 
                 :energy_choice, :temperament_choice, :image, :image_cache, :remote_image_url
  
  mount_uploader :image, ImageUploader

  #validates :animal_id, presence: true 
  validates :name, presence: true
  validates :age, presence: true
  validates :status, presence: true
  validates :gender, presence: true
  validates :weight, numericality: { only_integer: true }, presence: true
  validates :breed_type, presence: true 
  validates :breed_one, presence: true
  validates :breed_two, presence: true
  validates :cat_compatible, presence: true
  validates :dog_compatible, presence: true
  validates :human_compatible, presence: true
  validates :energy_level, presence: true
  validates :temperament, presence: true
  validates :fee, numericality: { only_integer: true }, presence: true
  validates :description, presence: true
  validates :image, presence: true

             

  private
    #Set default for secondary breed_type if purebred is selected
    def set_breedtype
      if breed_type == "Purebreed" 
        self.breed_two = "Not applicable"    
    end

    #Capitalize dog names with spaces
    def capitalize_dog_name
      new_name = name.split(" ").each{|i| i.capitalize!}.join(" ")
      self.name = new_name
    end

  end


  STATUS_CHOICE = ['Foster', 'Adoptable']
  GENDER_CHOICE = ['Neutered Male', 'Unaltered Male', 'Spayed Female', 'Unaltered Female']
  AGE_CHOICE = ['2 Months', '3 Months', '4 Months', '5 Months', '6 Months', '7 Months', '8 Months', '9 Months', '10 Months', '11 Months', '1 Year', '2 Years', 
    '3 Years', '4 Years', '5 Years', '6 Years', '7 Years', '8 Years', '9 Years', '10 Years', '11 Years', '12 Years']
  CAT_CHOICE = ['Unknown', 'Good with cats', 'Not good with cats']
  DOG_CHOICE = ['Unknown', 'Good with dogs', 'Good with male dogs only', 'Good with female dogs only', 'Not good with other dogs']
  HUMAN_CHOICE = ['Good with women only', 'Good with men only', 'Good with adults & kids', 'Not good with kids']
  ENERGY_CHOICE = ['Very low', 'Low', 'Average', 'High', 'Very high']
  TEMPERAMENT_CHOICE = ['Very submissive', 'Somewhat submissive', 'Average submissive', 'Somewhat dominant', 'Very dominant']
  
  BREED_CHOICE = 
  [
  'Not Applicable',
  'Affenpinscher', 
  'Afghan Hound ',
  'Airedale Terrier', 
  'Akita', 
  'Alaskan Malamute', 
  'American English Coonhound', 
  'American Eskimo Dog (Miniature)', 
  'American Eskimo Dog (Standard)', 
  'American Eskimo Dog (Toy)', 
  'American Foxhound', 
  'American Hairless Terrier',
  'American Staffordshire Terrier', 
  'American Water Spaniel', 
  'Anatolian Shepherd Dog', 
  'Australian Cattle Dog', 
  'Australian Shepherd', 
  'Australian Terrier', 
  'Basenji', 
  'Basset Hound', 
  'Beagle', 
  'Bearded Collie', 
  'Beauceron', 
  'Bedlington Terrier', 
  'Belgian Malinois', 
  'Belgian Sheepdog', 
  'Belgian Tervuren', 
  'Bergamasco', 
  'Berger Picard', 
  'Bernese Mountain Dog', 
  'Bichon FrisÈ', 
  'Black and Tan Coonhound', 
  'Black Russian Terrier', 
  'Bloodhound', 
  'Bluetick Coonhound', 
  'Boerboel', 
  'Border Collie', 
  'Border Terrier', 
  'Borzoi', 
  'Boston Terrier', 
  'Bouvier des Flandres', 
  'Boxer', 
  'Boykin Spaniel', 
  'Briard', 
  'Brittany', 
  'Brussels Griffon', 
  'Bull Terrier', 
  'Bull Terrier (Miniature)', 
  'Bulldog', 
  'Bullmastiff', 
  'Clumber Spaniel',
  'Cairn Terrier', 
  'Canaan Dog', 
  'Cane Corso', 
  'Cardigan Welsh Corgi', 
  'Cavalier King Charles Spaniel', 
  'Cesky Terrier', 
  'Chesapeake Bay Retriever', 
  'Chihuahua', 
  'Chinese Crested Dog', 
  'Chinese Shar Pei', 
  'Chinook', 
  'Chow Chow', 
  'Cirneco dellEtna', 
  'Clumber Spaniel', 
  'Cocker Spaniel', 
  'Collie', 
  'Coton de Tulear', 
  'Curly-Coated Retriever', 
  'Dachshund', 
  'Dalmatian', 
  'Dandie Dinmont Terrier', 
  'Doberman Pinscher', 
  'Dogue de Bordeaux', 
  'English Cocker Spaniel', 
  'English Foxhound', 
  'English Setter', 
  'English Springer Spaniel', 
  'English Toy Spaniel', 
  'Entlebucher Mountain Dog', 
  'Field Spaniel', 
  'Finnish Lapphund', 
  'Finnish Spitz', 
  'Flat-Coated Retriever', 
  'French Bulldog', 
  'German Pinscher', 
  'German Shepherd Dog', 
  'German Shorthaired Pointer', 
  'German Wirehaired Pointer', 
  'Giant Schnauzer', 
  'Glen of Imaal Terrier', 
  'Golden Retriever', 
  'Gordon Setter', 
  'Great Dane', 
  'Great Pyrenees', 
  'Greater Swiss Mountain Dog', 
  'Greyhound', 
  'Harrier', 
  'Havanese', 
  'Ibizan Hound', 
  'Icelandic Sheepdog', 
  'Irish Red and White Setter', 
  'Irish Setter', 
  'Irish Terrier', 
  'Irish Water Spaniel', 
  'Irish Wolfhound', 
  'Italian Greyhound', 
  'Japanese Chin', 
  'Keeshond', 
  'Kerry Blue Terrier', 
  'Komondor', 
  'Kuvasz', 
  'Labrador Retriever', 
  'Lagotto Romagnolo', 
  'Lakeland Terrier', 
  'Leonberger', 
  'Lhasa Apso', 
  'Lˆwchen', 
  'Miniature Schnauzer',
  'Maltese', 
  'Manchester Terrier', 
  'Mastiff', 
  'Miniature American Shepherd', 
  'Miniature Bull Terrier', 
  'Miniature Pinscher', 
  'Miniature Schnauzer', 
  'Neapolitan Mastiff', 
  'Newfoundland', 
  'Norfolk Terrier', 
  'Norwegian Buhund', 
  'Norwegian Elkhound', 
  'Norwegian Lundehund', 
  'Norwich Terrier', 
  'Nova Scotia Duck-Tolling Retriever', 
  'Old English Sheepdog', 
  'Otterhound', 
  'Papillon', 
  'Parson Russell Terrier', 
  'Pekingese', 
  'Pembroke Welsh Corgi', 
  'Petit Basset Griffon VendÈen', 
  'Pharaoh Hound', 
  'Plott', 
  'Pointer', 
  'Polish Lowland Sheepdog', 
  'Pomeranian', 
  'Poodle',  
  'Portuguese Podengo Pequeno', 
  'Portuguese Water Dog', 
  'Pug', 
  'Puli', 
  'Pyrenean Shepherd', 
  'Rat Terrier', 
  'Redbone Coonhound', 
  'Rhodesian Ridgeback', 
  'Rottweiler', 
  'Russell Terrier', 
  'St. Bernard', 
  'Saluki', 
  'Samoyed', 
  'Schipperke', 
  'Scottish Deerhound', 
  'Scottish Terrier', 
  'Sealyham Terrier', 
  'Shetland Sheepdog', 
  'Shiba Inu', 
  'Shih Tzu', 
  'Siberian Husky', 
  'Silky Terrier', 
  'Skye Terrier', 
  'Sloughi', 
  'Smooth Fox Terrier', 
  'Soft-Coated Wheaten Terrier', 
  'Spanish Water Dog', 
  'Staffordshire Bull Terrier', 
  'Standard Schnauzer', 
  'Sussex Spaniel', 
  'Swedish Vallhund', 
  'Tibetan Mastiff', 
  'Tibetan Spaniel', 
  'Tibetan Terrier', 
  'Toy Fox Terrier', 
  'Treeing Walker Coonhound', 
  'Vizsla', 
  'Weimaraner', 
  'Welsh Springer Spaniel', 
  'Welsh Terrier', 
  'West Highland White Terrier', 
  'Whippet', 
  'Wire Fox Terrier', 
  'Wirehaired Pointing Griffon', 
  'Wirehaired Vizsla', 
  'Xoloitzcuintli', 
  'Yorkshire Terrier'
  ]
end




