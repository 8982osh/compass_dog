Rails.application.routes.draw do
  


  #Contact Us Pg
  resources :contacts

  devise_for :users
  devise_for :views
  
  #Adoptable dogs on Michigan Pg
  get 'michigan' => 'dogs#michigan'
  post 'michigan' => 'dogs#michigan'

  #Adoptable dogs on Michigan Pg
  get 'foster' => 'dogs#foster'
  post 'foster' => 'dogs#foster'

  resources :dogs

  #Display Ways To Help Pg 
  get 'welcome/help'

  #Events Pg
  get 'welcome/events'

  #Display using carousel btn
  post 'welcome/events'

  get 'welcome/index'

  get 'welcome/about'

  #Button link
  post 'welcome/about'

  #Journey Pg
  get 'welcome/show'

  root to: 'welcome#index' 
end
