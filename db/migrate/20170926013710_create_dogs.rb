class CreateDogs < ActiveRecord::Migration
  def change
    create_table :dogs do |t|
      t.string :animal_id
      t.string :status
      t.string :name
      t.string :gender
      t.integer :weight
      t.string :age
      t.string :breed_one
      t.string :breed_two
      t.string :breed_type
      t.string :cat_compatible
      t.string :dog_compatible
      t.string :human_compatible
      t.string :energy_level
      t.string :temperament
      t.integer :fee
      t.text :description
      t.string :image

      t.timestamps null: false
    end
  end
end
