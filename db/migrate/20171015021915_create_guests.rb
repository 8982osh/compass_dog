class CreateGuests < ActiveRecord::Migration
  def change
    create_table :guests do |t|
      t.string :first_name
      t.string :last_name
      t.string :email
      t.string :subject
      t.text :descripton

      t.timestamps null: false
    end
  end
end
