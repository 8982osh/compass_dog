class RenameGuestsToContacts < ActiveRecord::Migration
  def change
  	rename_table :guests, :contacts
  end
end
